package edu.uchicago.gerber.andlab09.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import edu.uchicago.gerber.andlab09.bus.MyApplication;

/**
 * Created by agerber on 5/22/2016.
 */
public class MyReceiver extends BroadcastReceiver {




   //registered with <action android:name="android.provider.Telephony.SMS_RECEIVED" />
    @Override
    public void onReceive(Context context, Intent intent) {
        MyApplication.bus.register(this);
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        String message = "";
        if(bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];

            for(int i=0; i<msgs.length;i++) {
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                message = msgs[i].getMessageBody();
                Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
              //  MyApplication.bus.post(message);
            }

        }



    }
}