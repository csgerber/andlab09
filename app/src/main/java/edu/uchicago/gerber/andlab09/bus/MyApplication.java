package edu.uchicago.gerber.andlab09.bus;

import android.app.Application;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by agerber on 5/22/2016.
 */
public class MyApplication extends Application {
    public static Bus bus;
    @Override
    public void onCreate() {
        super.onCreate();
        bus = new Bus(ThreadEnforcer.ANY);
    }
}
