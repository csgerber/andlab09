package edu.uchicago.gerber.andlab09.binding;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.Date;

import edu.uchicago.gerber.andlab09.R;
import edu.uchicago.gerber.andlab09.bus.MyApplication;
import edu.uchicago.gerber.andlab09.limits.BackgroundService;

public class MainActivity extends Activity
        implements ServiceConnection, MyLocalService.Callback {
    private MyLocalService mService;


    private ProgressBar progressBar;
    private Button buttonService, buttonStop;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        MyApplication.bus.register(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        buttonService = (Button) findViewById(R.id.buttonService);
        buttonStop = (Button) findViewById(R.id.buttonStop);
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent svc=new Intent(MainActivity.this, BackgroundService.class);
                stopService(svc);
            }
        });

        buttonService.setEnabled(true);
        buttonService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent svc=new Intent(MainActivity.this, BackgroundService.class);
                startService(svc);

//                if(mService != null) {
//                  //  mService.performLongRunningOperation("1463772858498");
//
//
//
//
//                }
            }
        });




    }

    @Override
    protected void onResume() {
        super.onResume();


     //   Intent bindIntent = new Intent( MainActivity.this, MyLocalService.class);
       // bindService(bindIntent, MainActivity.this, BIND_AUTO_CREATE);





    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mService != null) {
            mService.setCallback(null); // Important to avoid memory leaks
            unbindService(this);
        }
    }


    @Override
    public void onOperationProgress(int progress) {
        // TODO Update user interface with progress..

      progressBar.setProgress(progress);

    }

    @Override
    public void onOperationCompleted(Date date) {
        Toast.makeText(this, date.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceConnected(ComponentName componentName,
                                   IBinder iBinder) {
        mService = ((MyLocalService.LocalBinder) iBinder).getService();
        mService.setCallback(this);

        // Once we have a reference to the service, we can update the UI and
        // enable buttons that should otherwise be disabled.

        buttonService.setEnabled(true);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        // Disable the button as we are loosing the reference to the service.
        mService = null;
        buttonService.setEnabled(false);

    }

    @Subscribe
    public void getMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }
}
