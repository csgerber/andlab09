package edu.uchicago.gerber.andlab09;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * @author Erik Hellman
 */
public class SharePhoto extends Activity {

    private Button trigger_operation_button;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_photo);

        trigger_operation_button = (Button) findViewById(R.id.trigger_operation_button);
        trigger_operation_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharePhoto( "descripton of photo");
            }
        });

    }

    // Activity code left out for brevity...

    public void sharePhoto(String description) {
        Intent sharePhotoIntent =
                new Intent(PhotoUploader.ACTION_SHARE_PHOTO);
//        sharePhotoIntent.
//                putExtra(PhotoUploader.EXTRA_PHOTO_BITMAP, photo);
        sharePhotoIntent.
                putExtra(PhotoUploader.EXTRA_PHOTO_TEXT, description);
        startService(sharePhotoIntent);
    }
}